import React from 'react';
import './style/css/App.css';
import user_white from './style/img/user_white.png'

function App() {
  return (
    <div className="App">
      <header className="header">
        <h1 className="header__title">e-List</h1>
      </header>
      <main className="login">
          <img className="login__img-main" src={user_white}></img>
            <form className="login__form">
              <h2 className="login__title">Log In:</h2>
              <input type="email" placeholder="Email" className="login__input-email"></input>
              <input type="password" placeholder="Password" className="login__input-password"></input>
              <div className="login__checkRecover">
                <label className="login__checkbox-label"><input type="checkbox" className="login__checkbox-input" />Recordar contraseña</label>
                <p className="login__recover">Recuperar contraseña</p>
              </div>
              <button type="submit" className="login__button-submit">Entrar</button>
            </form>
      </main>
      <footer className="footer">
        <p className="footer__text">2020 Todos los derechos reservados</p>
      </footer>
    </div>
  );
}

export default App;
